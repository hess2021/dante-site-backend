<?php
ignore_user_abort(true);

require 'php/arrays.php';
require 'php/functions.php';

// SANITIZE INPUTS
$searchRegex = '\'[^a-zA-Z0-9:, -]\'';  // search term
$cantoRegex = '\'[^0-9,]\'';		// canto selection
$rangeRegex = '\'[^0-9]\'';		// context range
$prefRegex = '\'[^on]\'';		// checkbox preference, like canticle selection
$radioRegex = '\'[^a-zA-Z]\''; 		// radio button value
$colorRegex = '\'[^a-zA-Z]\'';		// highlight colors

// Iterate through POST data and add to $safePOST
foreach ($_POST as $key => $userInput) {
	if ($key == 'searchTerm') {
		$safePOST[$key] = sanitize_value($userInput,$searchRegex);
	}
	elseif ($key == 'colorHighlight') {
		$safePOST[$key] = sanitize_value($userInput,$colorRegex);
	}
	elseif (preg_match('/([a-z]*Minus|[a-z]*Plus)/',$key)) {
		$safePOST[$key] = sanitize_value($userInput,$rangeRegex);
	}
	elseif (preg_match('/([a-z]*Cantos)/',$key)) {
		$safePOST[$key] = sanitize_value($userInput,$cantoRegex);
	}
	elseif (preg_match('/([a-z]*Context)/',$key)) {
		$safePOST[$key] = sanitize_value($userInput,$radioRegex);
	}
	else {
		$safePOST[$key] = sanitize_value($userInput,$prefRegex);
	}
}

// BUILD VARIABLES
$searchTerm = $safePOST['searchTerm'];

$infernoCanticle = $safePOST['infernoCanticle'];
$purgatorioCanticle = $safePOST['purgatorioCanticle'];
$paradisoCanticle = $safePOST['paradisoCanticle'];

// Explode the comma separated list into an array of canto filenames
$infernoCantos = cantos_to_files('inferno',$safePOST['infernoCantos']);
$purgatorioCantos = cantos_to_files('purgatorio',$safePOST['purgatorioCantos']);
$paradisoCantos = cantos_to_files('paradiso',$safePOST['paradisoCantos']);

$resultContext = $safePOST['resultContext'];

$lineMinus = $safePOST['lineMinus'];
$linePlus = $safePOST['linePlus'];

$tercetMinus = $safePOST['tercetMinus'];
$tercetPlus = $safePOST['tercetPlus'];

$boldHighlight = $safePOST['boldHighlight'];
$italicsHighlight = $safePOST['italicsHighlight'];
$underlineHighlight = $safePOST['underlineHighlight'];
$colorHighlight = $safePOST['colorHighlight'];

// DIRECTORY STRUCTURE
$time       = time();
$searchHash = hash('sha512',$time);
$shortHash  = substr($searchHash,0,6);
$hashDir    = "/var/www/dante-site-backend/searches/$shortHash";
$xmlFile   = "$hashDir/$shortHash.xml";
$xslFile    = "$hashDir/$shortHash.xsl";
$cssFile    = "$hashDir/$shortHash.css";
$comedyDir  = "/var/www/dante-site-backend/xml-commedia/comedy-xml.d";

if (!mkdir($hashDir, 0755)) {
	die('<br>Failed to create search directory. Try hitting "Submit" again.</br>');
}

// BUILD THE CANTO MEGAFILE
copy_base($xmlFile);

foreach ($canticles as $canticle) {
	$canticleSelection = "${canticle}Canticle";
	$cantoSelection = "${canticle}Cantos";
	if (${$canticleSelection} == 'on') {
		//$cmd = add_canticle($xmlFile,$comedyDir,$canticle);
		//echo "<br></br>xml command:<br>";
		//echo "<code>$cmd</code>";
		add_canticle($xmlFile,$comedyDir,$canticle);
	}
	elseif (!empty(${$cantoSelection})) {
		open_canticle($xmlFile,$canticle);
		$canticleDir = "$comedyDir/$canticle-xml.d";
		foreach (${$cantoSelection} as $cantoFile) {
			add_canto($xmlFile,$hashDir,$canticleDir,$cantoFile,$canticle);
		}
		close_canticle($xmlFile,$canticle);
		add_canto_tabs($xmlFile,$canticle);
	}
}

add_end_tags($xmlFile);

// BUILD THE XSLT

// Insert XSLT into $xmlFile
$xmlFileXML = file_get_contents($xmlFile);
$xmlFileXML = str_replace('XSLT_FILE',"$shortHash.xsl",$xmlFileXML);

// Remove namespace from XML because browsers use XSLT 1.0
// which won't recognize `xpath-default-namespace` in XSLT
// and we don't want to type the namespace for the whole xpath.
$xmlFileXML = str_replace(' xmlns="http://www.tei-c.org/ns/1.0"','',$xmlFileXML);
file_put_contents($xmlFile,$xmlFileXML);

// Copy XSLT to $hashDir and $shortHash.xsl
if ($resultContext == 'lineContext') {
	copy("templates/xslt/line-context.xsl",$xslFile);
}
elseif ($resultContext == 'tercetContext') {
	copy("templates/xslt/tercet-context.xsl",$xslFile);
}

// Substitute search term into xsl
$xslFileXSL = file_get_contents($xslFile);
$xslFileXSL = str_replace('WORD',$searchTerm,$xslFileXSL);
$xslFileXSL = str_replace('SHORTHASH',$shortHash,$xslFileXSL);
file_put_contents($xslFile,$xslFileXSL);


// BUILD THE CSS
copy("templates/css/highlight.css",$cssFile);
$cssFileCSS = file_get_contents($cssFile);
$highlightOpenTags = '';
$highlightCloseTags = '';

if ($boldHighlight == 'on') {
	$cssFileCSS = str_replace('FONT_WEIGHT','bold',$cssFileCSS);
}

if ($italicsHighlight == 'on') {
	$cssFileCSS = str_replace('FONT_STYLE','italic',$cssFileCSS);
}

if ($underlineHighlight == 'on') {
	$cssFileCSS = str_replace('TEXT_DECORATION','underline',$cssFileCSS);
}

if (!empty($colorHighlight)) {
	$cssFileCSS = str_replace('COLOR',$colorHighlight,$cssFileCSS);
}

//echo "<br>$shortHash";
//echo_arr("safe post",$safePOST);

file_put_contents($cssFile,$cssFileCSS);


// REDIRECT
header("Location: searches/$shortHash/$shortHash.xml");






/* CONTEXT - xpath
 *
 * line		match the parent of the word and return it
 * line±	match the parent of the word, get num attr, do math, match and return all xpaths
 * tercet	match the grandparent of the word and return it
 * tercet±	match the grandparent of the word, get num attr, do math, match and return all xpaths
 */


/* SELECTION - filenames
 * 
 * Canticles
 *   Inferno, Purgatorio, Paradiso
 *
 * Cantos
 *   1-34 per canticle; 1-100
 *   Must be processed after canticle selections
 */


/* HIGHLIGHT
 *
 * Context:
 *   word in line, tercet, canto
 *   line in tercet, canto
 */

/* XSLT: Highlight word in line
 *
 * Match the line containing the word.
 *   Apply the line template.
 *     Match the word.
 *       Apply the word template which puts the value of the match inside <high></high>
 */
?>
