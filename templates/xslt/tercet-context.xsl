<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
	This XSL returns the line± context of a word and places the word in <high></high> tags 
-->

<xsl:template match="TEI[text]">
	<html>
		<link rel="stylesheet" type="text/css" href="SHORTHASH.css"/>
		<body>
			<h1>
				Instances of "WORD".
			</h1>
			<table border="0">
				<xsl:for-each select="//w[text()='WORD']/../..">
				<!-- We need to add these variables by using XSL to get nodes and attributes.  -->
					<xsl:variable name="canticle">
					<!-- (w) l tercet tercets canto canticle text TEI -->
						<xsl:variable name="firstChar" select="substring(local-name(./../../..),1,1)"/>
						<xsl:value-of select="translate($firstChar,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/><xsl:value-of select="substring-after(local-name(./../../..),$firstChar)"/>
					</xsl:variable>
					<xsl:variable name="canto_num">
						<xsl:value-of select="./../../@canto_num"/>
					</xsl:variable>
					<xsl:variable name="tercet_num">
						<xsl:value-of select="./@t_num"/>
					</xsl:variable>
					<tr>
						<td style="vertical-align: top">
							<xsl:copy-of select="$canticle"/>&#160;<xsl:copy-of select="$canto_num"/>, tercet <xsl:copy-of select="$tercet_num"/>&#160;&#160;
						</td>
						<td style="vertical-align: top; line-height: 20px">
							<xsl:for-each select="l[position()=1]">
								<p><xsl:call-template name="firstTercetLine"/></p>
							</xsl:for-each>
							<xsl:for-each select="l[position()>1]">
								<p id="indent"><xsl:call-template name="indentTercetLine"/></p>
							</xsl:for-each>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</body>
	</html>
</xsl:template>

<xsl:template match="l" name="firstTercetLine">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="l" name="indentTercetLine">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="w[text()='WORD']">
	<highlight><xsl:value-of select="."/></highlight>
</xsl:template>

</xsl:stylesheet>
