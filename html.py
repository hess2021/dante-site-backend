"""
create html files for <canticle>-<sortType>.html to put on word-count.html main page
    the created files are lists with links to each canto in that canticle
        each canticle needs four files to be created to get all the sorting types:
            - numerical sort
            - numerical sort with blacklist
            - alphabetical sort
            - alphabetical sort with blacklist
this script was more quickly put together because it will presumably be rewritten to comply with each server's directory structure
"""
import os
import re

currentDir = os.path.abspath(os.getcwd())


def boilerplate(htmlFile, canticle, sortType):
    if sortType.split("-")[0] == "num":
        if "-" in sortType:
            numOrAlpha = "Numerically"
            blackOrNotTitle = "with Blacklist"
            blackOrNotHeading = " , with a blacklist applied"
        else:
            numOrAlpha = "Numerically"
            blackOrNotTitle = ""
            blackOrNotHeading = ""
    else:
        if "-" in sortType:
            numOrAlpha = "Alphabetically"
            blackOrNotTitle = "with Blacklist"
            blackOrNotHeading = " , with a blacklist applied"
        else:
            numOrAlpha = "Alphabetically"
            blackOrNotTitle = ""
            blackOrNotHeading = ""
    with open(htmlFile, "w") as html:
        html.write("<html>\n")
        html.write(" <head>\n")
        html.write('  <meta charset="UTF-8">\n')
        html.write("  <title>\n")
        #####################################################
        html.write(
            "   "
            + canticle.title()
            + " "
            + numOrAlpha
            + " Sorted"
            + blackOrNotTitle
            + "\n"
        )
        html.write("  </title>\n")
        html.write(" </head>\n")
        html.write(" <body>\n")
        #####################################################
        html.write(
            "  <h3>Cantos of <i>"
            + canticle.title()
            + "</i> with word count sorted "
            + numOrAlpha.lower()
            + blackOrNotHeading
            + ".</h3>\n"
        )
        html.write("  <ul>\n")
        print("added boilerplate")
    html.close()


def appendListElement(canto, htmlFile, canticle, sortType):
    cantoNum = str(int(canto.split("-")[0]))
    canticle = canto.split("-")[1]
    print(canticle + cantoNum)
    with open(htmlFile, "a") as html:
        #####################################################
        html.write(
            '   <li><a href="https://dante.lorenzohess.net:1534/xml-commedia/comedy-wordcount.d/html/'
            + canticle
            + "-"
            + sortType
            + "/"
            + canto
            + '"><i>'
            + canticle.title()
            + "</i> "
            + cantoNum
            + "</a></li>\n"
        )
        print("appended list element")
    html.close()


def boilerclose(htmlFile):
    with open(htmlFile, "a") as html:
        html.write("  </ul>\n")
        html.write(" </body>\n")
        html.write("</html>")
        print("appended boiler close")
    html.close()


def main():
    #####################################################
    canticles = ["inferno", "purgatorio", "paradiso"]
    sortTypes = ["num", "num-black", "alpha", "alpha-black"]
    for canticle in canticles:
        for sortType in sortTypes:
            if "-" in sortType:
                htmlFile = (
                    canticle
                    + "-"
                    + sortType.split("-")[0]
                    + "-sort-"
                    + sortType.split("-")[1]
                    + ".html"
                )
            else:
                htmlFile = canticle + "-" + sortType + "-sort.html"
            boilerplate(htmlFile, canticle, sortType)
            if canticle == "inferno":
                for i in range(1, 35):
                    print(canticle)
                    num = str(i).zfill(3)
                    canto = num + "-" + canticle + "-petrocchi-" + sortType + ".html"
                    appendListElement(canto, htmlFile, canticle, sortType)
            else:
                for i in range(1, 34):
                    print(canticle)
                    num = str(i).zfill(3)
                    canto = num + "-" + canticle + "-petrocchi-" + sortType + ".html"
                    appendListElement(canto, htmlFile, canticle, sortType)
            boilerclose(htmlFile)


main()
