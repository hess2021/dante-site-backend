<?php

require('php/functions.php');

// SANITIZE INPUTS
$searchRegex = '\'[^a-zA-Z0-9:, -]\'';  // search term

// Iterate through POST data and add to $safePOST
foreach ($_POST as $key => $userInput) {
	if ($key == 'searchTerm') {
		$safePOST[$key] = sanitize_value($userInput,$searchRegex);
	}
}

// BUILD VARIABLES
$searchTerm = $safePOST['searchTerm'];
$csvFile    = "/var/www/dante-site-backend/xml-commedia/comedy-wordcount.d/csv/word-per-canto/" . $searchTerm . ".csv";

if (file_exists($csvFile)) {
	echo "<html><body><table><tr><td><strong>Canticle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td><strong>Canto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td><strong>Word count</strong></td></tr>\n\n";
	$f = fopen($csvFile, "r");
	while (($line = fgetcsv($f)) !== false) {
		echo "<tr>";
		foreach ($line as $cell) {
			echo "<td>" . htmlspecialchars($cell) . "</td>";
		}
		echo "</tr>\n";
	}
	fclose($f);
	echo "\n</table></body></html>";
}
else {
	die('<strong>This word is not in the <i>Comedy</i>, or perhaps it hasn\'t been correctly indexed by this system.</strong>');
}



?>
