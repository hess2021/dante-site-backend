<?php

/* Print a more easily readable array (debugging only) */
function echo_arr($name,$arr){
        echo "<br><br><strong>$name</strong><br>";
        echo "<pre>"; print_r($arr); echo "</pre";
}

/* Sanitize a value given a regex */
function sanitize_value($evilValue,$regex){
        $safeValue = preg_replace($regex,'',$evilValue);
        return $safeValue;
}

/* Copy base XML template for canto megafile */
function copy_base($megafile){
	copy("templates/xml/megafile-base.xml",$megafile);
}

/* Explode comma-separated canto list to array with full canto names */
// 0xx-canticle-petrocchi.xml
function cantos_to_files($canticle,$canticleCantos){
	if ($canticleCantos != '') {
		$base = "-$canticle-petrocchi.xml";
		$canto_nums = explode(',',$canticleCantos); // array
		foreach ($canto_nums as $canto_num) {
			$padded_num = sprintf('%03d',$canto_num);
			$canto_file = "$padded_num$base";
			$canticleCantosArray[] = $canto_file;
		}
		return $canticleCantosArray;
	}
	else {
		return '';
	}
}

function add_canticle($megafile,$comedyDir,$canticle){
	// For some reason the second and third canticles are not added with a newline.
	// So we need a workaround of adding a newline to the closing tag.
	$xmlstarletCmd = "cd $comedyDir && xmlstarlet sel -t -c '_:TEI/_:text/_:$canticle' $canticle-complete.xml | sed 's/<$canticle\ xmlns=\"http:\/\/www.tei-c.org\/ns\/1.0\"/\\t\\t<$canticle/' | sed 's/<\/$canticle>/<\/$canticle>\\n/' >> $megafile";
	shell_exec($xmlstarletCmd);
	//return $xmlstarletCmd;
}

function open_canticle($megafile,$canticle){
	$canticleTagOpen = "<$canticle>";
	$echoCmd = "echo '		$canticleTagOpen' >> $megafile";
	shell_exec($echoCmd);
}

function close_canticle($megafile,$canticle){
	$canticleTagClose = "</$canticle>";
	$echoCmd = "echo '		$canticleTagClose' >> $megafile";
	shell_exec($echoCmd);
}

function add_canto($megafile,$hashDir,$canticleDir,$cantoFile,$canticle){
	// Add canto <text> contents
	$xmlstarletCmd = "cd $canticleDir && xmlstarlet sel -t -c '_:TEI/_:text/_:canto' $cantoFile | sed 's/<canto\ xmlns=\"http:\/\/www.tei-c.org\/ns\/1.0\"/\\t\\t<canto/' | sed 's/<\/canto>/<\/canto>\\n/' >> $megafile";
	shell_exec($xmlstarletCmd);
}

function add_canto_tabs($megafile,$canticle){
	// Add a tab in front of every line of the added cantos
	$startLine = shell_exec("grep -n '<$canticle>' $megafile | tr -d -c 0-9");
	$endLine   = shell_exec("grep -n '</$canticle>' $megafile | tr -d -c 0-9");
	// Exclude canticle tags
	++$startLine;
	--$endLine;
	$tabCmd = "sed -i '$startLine,${endLine}s/^/\\t/g' $megafile";
	shell_exec($tabCmd);
}

function add_end_tags($megafile){
	// Use sed because `echo -e` adds `-e` for some reason.
	$endTagsCmd = "sed -i '$ a \\\t</text>\\\n</TEI>'  $megafile";
	shell_exec($endTagsCmd);
}

?>
